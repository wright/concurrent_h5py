# concurrent_h5py

Test project to investigate concurrent reading and writing with h5py.

Run the tests:

```bash
   ./test.sh
```

Based on https://gitlab.esrf.fr/wright/rwh5bug/
