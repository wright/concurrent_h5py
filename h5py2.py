# coding: utf-8
# /*##########################################################################
# Copyright (C) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/
"""
This module contains wrapper for h5py. The exposed layout is
as close as possible to the original file format.
"""

__authors__ = ["W. de Nolf"]
__license__ = "MIT"
__date__ = "20/01/2020"


import os
import time
from functools import wraps
from contextlib import contextmanager, ExitStack
import traceback
import errno
import threading
import h5py


HASSWMR = h5py.version.hdf5_version_tuple >= h5py.get_config().swmr_min_hdf5_version
RETRY_PERIOD = 0.001


class HDF5TimeoutError(TimeoutError):
    pass


def isErrno(e, errno):
    """
    :param OSError e:
    :returns bool:
    """
    # Because e.__cause__ is None for chained exceptions
    return "errno = {}".format(errno) in "".join(traceback.format_exc())


class SharedLockPool:
    """
    Allows to acquire locks identified by name (hashable type) recursively.
    """

    LOCK_CLASS = threading.Lock
    RLOCK_CLASS = threading.RLock

    def __init__(self):
        self.__locks = {}
        self.__locks_mutex = self.LOCK_CLASS()

    def __len__(self):
        return len(self.__locks)

    @property
    def names(self):
        return list(self.__locks.keys())

    @contextmanager
    def _modify_locks(self):
        self.__locks_mutex.acquire()
        try:
            yield self.__locks
        finally:
            self.__locks_mutex.release()

    @contextmanager
    def acquire(self, name):
        with self._modify_locks() as locks:
            lock = locks.get(name, None)
            if lock is None:
                locks[name] = lock = self.RLOCK_CLASS()
        lock.acquire()
        try:
            yield
        finally:
            lock.release()
            with self._modify_locks() as locks:
                locks.pop(name)

    @contextmanager
    def acquire_context_creation(self, name, contextmngr, *args, **kwargs):
        """
        Acquire lock only during context creation.

        This can be used for example to protect the opening of a file
        but not hold the lock while the file is open.
        """
        with ExitStack() as stack:
            with self.acquire(name):
                ret = stack.enter_context(contextmngr(*args, **kwargs))
            yield ret


def retry(timeout=None, context=False, retry_period=None, **kw):
    """Decorate method that open+read an HDF5 file that is being written too.
    When HDF5 IO fails (because the writer is modifying the file) the method
    will be retried.

    Note: the method needs to be idempotent.
    """

    # TODO: better way to detect concurrency errors

    btimeout = timeout is not None
    if retry_period is None:
        retry_period = RETRY_PERIOD

    def decorator(method):
        @wraps(method)
        def wrapper(filename, *args, **_kw):
            if btimeout:
                t0 = time.time()
            while True:
                try:
                    with File(filename, **kw) as f:
                        result = method(f, *args, **_kw)
                        if context:
                            yield next(result)
                            return
                        else:
                            return result
                except HDF5TimeoutError:
                    raise
                except OSError:
                    pass
                except RuntimeError:
                    pass
                except KeyError as e:
                    if "doesn't exist" in str(e):
                        raise
                if retry_period >= 0:
                    time.sleep(retry_period)
                if btimeout and (time.time() - t0) > timeout:
                    raise HDF5TimeoutError

        if context:
            return contextmanager(wrapper)
        else:
            return wrapper

    return decorator


def default_validate_group(grp):
    return "end_time" in grp


@retry(timeout=None, context=True)
def top_level_groups(h5file, validate_group=default_validate_group):
    if callable(validate_group):
        yield [h5file[grp] for grp in h5file["/"] if validate_group(h5file[grp])]
    else:
        yield [h5file[grp] for grp in h5file["/"]]


@retry(timeout=None, context=True)
def open(h5file):
    yield h5file


class File(h5py.File):
    _LOCKPOOL = SharedLockPool()

    def __init__(
        self, filename, mode=None, enable_file_locking=None, swmr=None, **kwargs
    ):
        """
        :param str filename:
        :param str mode:
        :param bool enable_file_locking: by default it is disabled for `mode=='r'`
                                         and enabled in all other modes
        :param bool swmr: when not specified: try both modes when `mode=='r'`
        :param **kwargs: see `h5py.File.__init__`
        """
        if mode is None:
            mode = "r"
        elif mode not in ("r", "w", "w-", "x", "a"):
            raise ValueError("invalid mode {}".format(mode))
        if not HASSWMR and swmr:
            swmr = False
        libver = kwargs.setdefault("libver", "v110")
        if enable_file_locking is None:
            enable_file_locking = mode != "r"
        kwargs.setdefault("track_order", True)

        with ExitStack() as stack:
            ctx = self._protect_init(filename)
            stack.enter_context(ctx)

            ctx = self._hdf5_locking_context(enable_file_locking)
            stack.enter_context(ctx)

            try:
                fileobj = super().__init__(filename, mode=mode, swmr=swmr, **kwargs)
                if mode != "r" and swmr:
                    # Try setting writer in SWMR mode
                    try:
                        fileobj.swmr_mode = True
                    except Exception:
                        pass
                return fileobj
            except OSError as e:
                if (
                    swmr is not None
                    or mode != "r"
                    or not HASSWMR
                    or not isErrno(e, errno.EAGAIN)
                ):
                    raise
                # Try reading with opposite SWMR mode
                swmr = not swmr
                if swmr:
                    kwargs["libver"] = "latest"
                else:
                    kwargs["libver"] = libver
                super().__init__(filename, mode=mode, swmr=swmr, **kwargs)

    @contextmanager
    def _hdf5_locking_context(self, enable_file_locking):
        old_file_locking = os.environ.get("HDF5_USE_FILE_LOCKING", None)
        if enable_file_locking:
            os.environ["HDF5_USE_FILE_LOCKING"] = "TRUE"
        else:
            os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
        try:
            yield
        finally:
            if old_file_locking is None:
                del os.environ["HDF5_USE_FILE_LOCKING"]
            else:
                os.environ["HDF5_USE_FILE_LOCKING"] = old_file_locking

    @contextmanager
    def _protect_init(self, filename):
        """Makes sure no other file is opened/created
        or protected sections associated to the filename
        are executed.
        """
        lockname = os.path.abspath(filename)
        with self._LOCKPOOL.acquire(None):
            with self._LOCKPOOL.acquire(lockname):
                yield

    @contextmanager
    def protect(self):
        """Protected section associated to this file."""
        lockname = os.path.abspath(self.filename)
        with self._LOCKPOOL.acquire(lockname):
            yield
