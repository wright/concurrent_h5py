"""Test processing an HDF5 file that is being written

Launch the writer (only one):

    python safe_open_hdf5.py --writer

Launch the readers (as many as you want):

    python safe_open_hdf5.py
"""

import os
import sys
import time
import datetime
import signal
import numpy
import logging
from contextlib import contextmanager

# from silx.io import h5py
import h5py2 as h5py


logging.basicConfig(
    level=logging.INFO, format=f"%(asctime)s - [pid={os.getpid()}] %(message)s"
)


logger = logging.getLogger()
# Write more datasets
NPT = 10
NROW = 5
NDATASET = 3
TEST_DATA = numpy.arange(0, NROW * NPT).reshape(NROW, NPT)

FLUSH_PERIOD = 0.5


def process_scan(scan):
    logger.info("reading scan " + scan.name)
    if "end_time" not in scan:
        logger.info("Scan not processed (not complete yet)")
        return
    measurement = scan["measurement"]
    for k in measurement:
        numpy.testing.assert_array_equal(measurement[k][()], TEST_DATA)


def reader_processed():
    processed = []

    def signal_handler(sig, frame):
        logger.info("stopped reading: " + str(len(processed)) + " scans processed")
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    return processed


def reader(filename):
    """Main reader loop
    """
    while True:
        with h5py.top_level_groups(filename) as scans:
            for ns,scan in enumerate(scans):
                try: # always read the data
                    process_scan(scan)
                except Exception:
                    logger.error(scan)
                    raise
        logger.info(str(ns) + " scans processed")


def get_scan_names(filename):
    sname = filename + "_scans.log"
    if os.path.exists(sname):
        return [s.rstrip() for s in open(sname, "r").readlines()]
    else:
        return []


@h5py.retry(timeout=None, context=True)
def top_level_names(h5file):
    names = get_scan_names(h5file.filename)
    yield h5file, names


def reader_with_scanfile(filename):
    """Main reader loop
    """
    processed = reader_processed()
    while True:
        with top_level_names(filename) as (f, names):
            for name in names:
                if name in processed:
                    continue
                try:
                    scan = f[name]
                    process_scan(scan)
                except Exception:
                    logger.error(name)
                    raise
                processed.append(name)
        logger.info(str(len(processed)) + " scans processed")


def write_scan(f, scan_name, resize=True, compression="gzip"):
    logger.info("writing scan " + scan_name)
    scan = f.create_group(scan_name)
    scan.attrs["NX_class"] = "NXentry"
    scan["start_time"] = datetime.datetime.now().isoformat()
    measurement = scan.create_group("measurement")
    measurement.attrs["NX_class"] = "NXcollection"

    t0 = time.time()

    if resize:
        shape = (1, TEST_DATA.shape[1])
        maxshape = (None, TEST_DATA.shape[1])
    else:
        shape = TEST_DATA.shape
        maxshape = None

    datasets = []
    for i in range(NDATASET):
        dset = measurement.create_dataset(
            "data" + str(i), shape=shape, maxshape=maxshape, compression=compression
        )
        datasets.append(dset)
        dset[0] = TEST_DATA[0]
        if time.time() - t0 > FLUSH_PERIOD:
            f.flush()
            t0 = time.time()
            logger.info("flush")
    f.flush()
    logger.info("datasets created")
    # data written progressively
    for j in range(1, TEST_DATA.shape[0]):
        for dset in datasets:
            if resize:
                dset.resize((j + 1, TEST_DATA.shape[1]))
            dset[j] = TEST_DATA[j]
        if time.time() - t0 > FLUSH_PERIOD:
            f.flush()
            t0 = time.time()
            logger.info("flush")
    logger.info("data saved")

    scan["end_time"] = datetime.datetime.now().isoformat()
    f.flush()
    logger.info("finished writing scan " + scan_name)


def writer(filename, n=2000):
    """Main writer loop
    """
    try:
        os.unlink(filename)
    except FileNotFoundError:
        pass
    for j in range(n):
        with h5py.File(filename, mode="a", track_order=True) as f:
            scans = list(f["/"])
            if scans:
                scan_number = int(scans[-1].split(".")[0]) + 1
            else:
                f.attrs["NX_class"] = "NXroot"
                scan_number = 1
            scan_name = str(scan_number) + ".1"
            write_scan(f, scan_name)
            with open(filename + "_scans.log", "a") as scanfile:
                scanfile.write("%s\n" % (scan_name))

    logger.info("Finished writing")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Test processing an HDF5 which is being written."
    )
    parser.add_argument(
        "--writer", action="store_true", help="Run a writer (do only once)"
    )
    parser.add_argument(
        "--usescanfile", action="store_true", help="Use the scan file for reading"
    )
    args = parser.parse_args()
    if args.writer:
        writer("test.h5")
    elif args.usescanfile:
        reader_with_scanfile("test.h5")
    else:
        reader("test.h5")
