#!/bin/bash


spin() {
   local -a marks=( '/' '-' '\' '|' )
   while [[ 1 ]]; do
     printf '%s\r' "${marks[i++ % ${#marks[@]}]}"
     sleep 1
   done
 }


function main(){
    local reader_args=""

    while getopts "ht" opt; do
      case $opt in
        t)
          reader_args="--usescanfile"
          ;;
        h)
          echo "Usage:" >&2
          echo " ./test.sh [-t]" >&2
          exit 0
          ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          exit 1
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          exit 1
          ;;
      esac
    done

    rm -f *.log *.h5

    sleep 1

    # get the core
    ulimit -c 100000000

    # Single writer
    python3 safe_open_hdf5.py --writer 2>&1 &

    # Multiple readers
    for i in {1..22}; do
        python3 safe_open_hdf5.py $reader_args 2>&1 &
    done
    #spin &

    trap 'killing $(jobs -p);kill $(jobs -p)' INT
    
    wait -n
    echo "Some subprocess stopped, killing $(jobs -p)"
    kill $(jobs -p)
}

main $@
